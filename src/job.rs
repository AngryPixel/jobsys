use std::marker::PhantomData;
use std::sync::atomic::{AtomicI32, AtomicU32, Ordering};

pub(crate) const MAX_CHAINED_JOB_COUNT: usize = 8;

#[cfg(feature = "job_storage_64")]
const JOB_STORAGE_SIZE: usize = 64;

#[cfg(feature = "job_storage_128")]
const JOB_STORAGE_SIZE: usize = 128;

/// Store the run time data for our job closures
#[repr(align(8))]
struct JobStorage {
    data: [u8; JOB_STORAGE_SIZE],
    has_data: std::sync::atomic::AtomicBool,
}

impl JobStorage {
    fn new() -> Self {
        Self {
            data: [0; JOB_STORAGE_SIZE],
            has_data: std::sync::atomic::AtomicBool::new(false),
        }
    }
}

/// Check if we can store the given type in our storage space.
pub(crate) fn can_store_type<T>() -> bool
where
    T: Sized,
{
    let self_alignment = std::mem::align_of::<JobStorage>();
    let type_alignment = std::mem::align_of::<T>();
    let type_size = std::mem::size_of::<T>();
    if type_alignment <= self_alignment {
        // If the alignment of the type <= our alignment then will fit as long as the size
        // of the type is smaller than our capacity
        type_size <= JOB_STORAGE_SIZE
    } else {
        // If the type has greater capacity, we also need to account for fact that the
        // returned pointer needs to be properly aligned so we will lose at the maximum
        // the alignment of the type - our own alignment bytes.
        type_size + (type_alignment - self_alignment) <= JOB_STORAGE_SIZE
    }
}

/// This trait is used to type erase the job closure so we can access and drop the data later
trait StorageViewTrait {
    fn do_run(s: &mut JobStorage);
    fn do_drop(s: &mut JobStorage);
}

/// View of the job storage which maps to the corresponding closure type T
struct StorageView<'a, T> {
    storage: &'a mut JobStorage,
    phantom: PhantomData<T>,
}

impl<'a, T> StorageView<'a, T>
where
    T: 'a + Sized + FnOnce(),
{
    fn new(storage: &'a mut JobStorage) -> Self {
        Self {
            storage,
            phantom: PhantomData,
        }
    }
    /// Move the closure into the storage
    fn write(&mut self, t: T) {
        if self
            .storage
            .has_data
            .load(std::sync::atomic::Ordering::Acquire)
        {
            panic!("Over writing existing job data");
        }
        let ptr = self.storage.data.as_mut_ptr();
        let alignment = std::mem::align_of::<T>();
        let alignment_offset = ptr.align_offset(alignment);
        unsafe {
            std::ptr::write(ptr.add(alignment_offset) as *mut T, t);
        }
        self.storage
            .has_data
            .store(true, std::sync::atomic::Ordering::Release);
    }

    /// Move the closure out of the storage
    fn unwrap(&mut self) -> Option<T> {
        if self
            .storage
            .has_data
            .load(std::sync::atomic::Ordering::Acquire)
        {
            let ptr = self.storage.data.as_mut_ptr();
            let alignment = std::mem::align_of::<T>();
            let alignment_offset = ptr.align_offset(alignment);
            self.storage
                .has_data
                .store(false, std::sync::atomic::Ordering::Release);
            return Some(unsafe {
                // This is safe to do since we have guaranteed that there is actual data
                // in the store block
                std::ptr::read(ptr.add(alignment_offset) as *mut T)
            });
        }
        None
    }

    /// Call drop on the closure, if it is still present in the storage
    fn drop(&mut self) {
        if self
            .storage
            .has_data
            .load(std::sync::atomic::Ordering::Acquire)
        {
            let ptr = self.storage.data.as_mut_ptr();
            let alignment = std::mem::align_of::<T>();
            let alignment_offset = ptr.align_offset(alignment);
            unsafe {
                // This is safe to call since we keep track of which storage units
                // still have data. In other words they were not executed.
                let r = ptr.add(alignment_offset) as *mut T;
                r.drop_in_place();
            }
        }
    }
}

/// Generate the static functions we use for type erasure
impl<'a, T> StorageViewTrait for StorageView<'a, T>
where
    T: 'a + Sized + FnOnce(),
{
    fn do_run(s: &mut JobStorage) {
        let mut view = StorageView::<T>::new(s);
        if let Some(value) = view.unwrap() {
            (value)();
        } else {
            panic!("Attempting to run a job without any data");
        }
    }
    fn do_drop(s: &mut JobStorage) {
        let mut view = StorageView::<T>::new(s);
        view.drop();
    }
}

struct JobFunctions {
    fn_run: fn(&mut JobStorage),
    fn_drop: fn(&mut JobStorage),
}

/// Represents the memory storage for a Job.
#[repr(align(64))]
pub(crate) struct Job {
    function: Option<JobFunctions>,
    pub(crate) unfinished_count: AtomicI32,
    pub(crate) chained_job_count: AtomicU32,
    job_storage: JobStorage,
    // We store pointer here since these jobs will be queued after the job finishes and
    // we can't guarantee that the job thread which will execute them will have access to the job
    // pool. These are all safe to dereference as long as the job pool is filled over capacity
    pub(crate) parent: Option<std::ptr::NonNull<Job>>,
    pub(crate) chained_jobs: [std::ptr::NonNull<Job>; MAX_CHAINED_JOB_COUNT],
}

impl Job {
    pub(crate) fn new() -> Self {
        Self {
            function: None,
            unfinished_count: AtomicI32::new(0),
            chained_job_count: AtomicU32::new(0),
            job_storage: JobStorage::new(),
            parent: None,
            chained_jobs: [std::ptr::NonNull::dangling(); MAX_CHAINED_JOB_COUNT],
        }
    }
    pub(crate) fn store<T>(&mut self, data: T)
    where
        T: Sized + FnOnce(),
    {
        self.function = Some(JobFunctions {
            fn_run: StorageView::<T>::do_run,
            fn_drop: StorageView::<T>::do_drop,
        });
        let mut view = StorageView::<T>::new(&mut self.job_storage);
        view.write(data);
    }

    pub(crate) fn run(&mut self) {
        if let Some(functions) = &mut self.function {
            (functions.fn_run)(&mut self.job_storage);
        }
        self.finish();
    }

    pub(crate) fn is_finished(&self) -> bool {
        self.unfinished_count.load(Ordering::Acquire) <= 0
    }

    pub(crate) fn reset(&mut self) {
        self.unfinished_count.store(1, Ordering::Release);
        self.chained_job_count.store(0, Ordering::Release);
        self.function = None;
        self.parent = None;
    }

    fn finish(&mut self) {
        if self.unfinished_count.fetch_sub(1, Ordering::AcqRel) == 1 {
            if let Some(functions) = &mut self.function {
                (functions.fn_drop)(&mut self.job_storage);
            }
            self.function = None;
        }
    }

    pub(crate) fn set_parent_job(&mut self, parent_job: std::ptr::NonNull<Job>) {
        unsafe {
            // This is safe since the memory address of the job is not going to be
            // moved/deallocate while the job system is running
            parent_job.as_ref()
        }
        .unfinished_count
        .fetch_add(1, Ordering::AcqRel);
        self.parent = Some(parent_job);
    }

    pub(crate) fn chain_job(&mut self, job: std::ptr::NonNull<Job>) -> Result<(), ()> {
        let index = self
            .chained_job_count
            .fetch_add(1, std::sync::atomic::Ordering::AcqRel) as usize;
        if index >= (MAX_CHAINED_JOB_COUNT - 1) as usize {
            return Err(());
        }
        self.chained_jobs[index] = job;
        Ok(())
    }
}

pub(crate) struct JobPool {
    jobs: Vec<Job>,
    allocated_count: usize,
}

impl JobPool {
    pub(crate) fn new(capacity: usize) -> Self {
        debug_assert!(capacity.is_power_of_two());
        let mut data = Vec::<Job>::with_capacity(capacity);
        data.resize_with(capacity, Job::new);
        Self {
            jobs: data,
            allocated_count: 0,
        }
    }

    pub(crate) fn allocate(&mut self) -> (&mut Job, usize) {
        self.allocated_count = self.allocated_count.wrapping_add(1);
        let index = self.allocated_count & (self.jobs.len() - 1);
        let job = &mut self.jobs[index];
        if !job.is_finished() {
            panic!("Allocating job with unfinished work, we may have run out of job handles");
        }
        job.reset();
        (job, index)
    }

    pub(crate) fn get_mut(&mut self, job_index: usize) -> Option<&'_ mut Job> {
        if job_index >= self.jobs.len() {
            None
        } else {
            Some(&mut self.jobs[job_index])
        }
    }

    pub(crate) fn get_mut_ptr(&mut self, job_index: usize) -> Option<std::ptr::NonNull<Job>> {
        if job_index >= self.jobs.len() {
            None
        } else {
            std::ptr::NonNull::new(&mut self.jobs[job_index])
        }
    }

    pub(crate) fn get(&self, job_index: usize) -> Option<&'_ Job> {
        if job_index >= self.jobs.len() {
            None
        } else {
            Some(&self.jobs[job_index])
        }
    }
}

impl Drop for JobPool {
    fn drop(&mut self) {
        for job in self.jobs.iter_mut() {
            if let Some(data) = &mut job.function {
                (data.fn_drop)(&mut job.job_storage);
            }
        }
    }
}
