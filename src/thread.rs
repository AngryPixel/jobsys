use crate::job::*;
use crate::queues::*;
use rand;
use rand::Rng;
use std::borrow::Borrow;
use std::cell::{Cell, RefCell};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

#[cfg(feature = "debug_queue")]
type QueueType = WorkStealingJobQueue;

#[cfg(not(feature = "debug_queue"))]
type QueueType = LocklessWorkStealingJobQueue;

#[derive(Debug)]
pub(crate) struct ThreadData {
    pub(crate) job_queue: QueueType,
    pub(crate) quit_flag: AtomicBool,
}

impl ThreadData {
    pub(crate) fn new(capacity: usize) -> Self {
        Self {
            job_queue: QueueType::new(capacity),
            quit_flag: AtomicBool::new(false),
        }
    }
}

thread_local! {
    static TLS_THREAD_DATA_INDEX: Cell<usize>= Cell::new(usize::MAX);
    static TLS_RANDOM_GEN: RefCell<rand::rngs::ThreadRng> = RefCell::new(rand::rngs::ThreadRng::default());
    static TLS_JOB_POOL: RefCell<Option<JobPool>> = RefCell::new(None);
    static TLS_THREAD_DATA : RefCell<Option<Arc<Vec<ThreadData>>>> = RefCell::new(None);
}

fn tls_get_thread_index() -> usize {
    TLS_THREAD_DATA_INDEX.with(|tls| -> usize { tls.get() })
}

fn tls_set_thread_index(index: usize) {
    TLS_THREAD_DATA_INDEX.with(|it| {
        // Quickly verify that this thread data was never initialized
        debug_assert!(it.borrow().get() == usize::MAX);
        it.set(index);
    });
}

fn tls_reset_thread_index() {
    TLS_THREAD_DATA_INDEX.with(|it| {
        it.set(usize::MAX);
    });
}

/// Try to set the main thread tls index, if this fails, it means that the there is more
/// than one instance of a job system running on this thread.
pub(crate) fn tls_set_main_thread_index() -> bool {
    if tls_get_thread_index() == usize::MAX {
        tls_set_thread_index(0_usize);
        return true;
    }
    false
}

pub(crate) fn tls_reset_main_thread_index() {
    tls_reset_thread_index();
}

pub(crate) fn tls_setup_job_pool(capacity: usize) {
    TLS_JOB_POOL.with(|tls| {
        let mut pool = tls.borrow_mut();
        // Should be the None
        debug_assert!(pool.is_none());
        *pool = Some(JobPool::new(capacity));
    });
}

pub(crate) fn tls_reset_job_pool() {
    TLS_JOB_POOL.with(|tls| {
        *tls.borrow_mut() = None;
    });
}

pub(crate) fn tls_get_job_pool() -> &'static std::thread::LocalKey<RefCell<Option<JobPool>>> {
    &TLS_JOB_POOL
}

pub(crate) fn tls_set_thread_data(data: Arc<Vec<ThreadData>>) {
    TLS_THREAD_DATA.with(|tls| {
        let mut thread_data = tls.borrow_mut();
        debug_assert!(thread_data.is_none());
        *thread_data = Some(data)
    })
}

pub(crate) fn tls_reset_thread_data() {
    TLS_THREAD_DATA.with(|tls| {
        *tls.borrow_mut() = None;
    })
}

pub(crate) fn tls_get_thread_data() -> Option<Arc<Vec<ThreadData>>> {
    TLS_THREAD_DATA.with(|tls| {
        let thread_data = tls.borrow();
        if let Some(data) = thread_data.as_ref() {
            return Some(data.clone());
        }
        None
    })
}

/// Represents a thread of the job system
pub(crate) struct JobThread {
    thread_data: std::sync::Arc<Vec<ThreadData>>,
    thread_index: usize,
    thread_handle: Option<std::thread::JoinHandle<()>>,
}

impl JobThread {
    pub(crate) fn new(data: std::sync::Arc<Vec<ThreadData>>, index: usize) -> Self {
        // Index 0 is reserved for the thread which creates the job system.
        debug_assert!(index > 0);
        Self {
            thread_data: data,
            thread_index: index,
            thread_handle: None,
        }
    }

    pub(crate) fn start(&mut self, capacity: usize) -> Result<(), std::io::Error> {
        debug_assert!(self.thread_index != 0);
        let thread = {
            let thread_index = self.thread_index;
            let thread_data = self.thread_data.clone();
            std::thread::Builder::new().spawn(move || {
                tls_set_thread_index(thread_index);
                tls_set_thread_data(thread_data.clone());
                tls_setup_job_pool(capacity);
                let this_thread_data = &thread_data[thread_index];
                while !this_thread_data.quit_flag.load(Ordering::Acquire) {
                    let mut opt_job = get_job(&thread_data);
                    if let Some(job) = opt_job.take() {
                        run_job(&thread_data, job);
                    }
                }
                tls_reset_job_pool();
                tls_reset_thread_index();
                tls_reset_thread_data();
            })
        };
        match thread {
            Ok(handle) => {
                self.thread_handle = Some(handle);
                Ok(())
            }
            Err(error) => Err(error),
        }
    }

    pub(crate) fn finish(&mut self) -> Result<(), ()> {
        self.thread_data[self.thread_index]
            .quit_flag
            .store(true, Ordering::Release);
        if let Some(handle) = self.thread_handle.take() {
            // We have nothing to report on error, other than the
            // thread joining failed.
            let r = handle.join();
            if r.is_err() {
                return Err(());
            }
        }
        Ok(())
    }
}

/// Try to obtain a new job to execute. First look into the thread's own queue. If that fails try to
/// steal a job form a random thread.
pub(crate) fn get_job(list: &std::sync::Arc<Vec<ThreadData>>) -> Option<std::ptr::NonNull<Job>> {
    let this_thread_index = tls_get_thread_index();
    let thread_data = &list[this_thread_index];
    let mut opt_job = thread_data.job_queue.pop();
    if opt_job.is_none() {
        let random_index =
            TLS_RANDOM_GEN.with(|rand| rand.borrow_mut().gen::<usize>() % list.len());
        if random_index == this_thread_index {
            std::thread::yield_now();
            return None;
        }
        let mut other_thread_job = thread_data.job_queue.steal();
        if other_thread_job.is_none() {
            std::thread::yield_now();
            return None;
        }
        opt_job = other_thread_job.take();
    };
    opt_job
}

/// Push a job to the thread's local queue
pub(crate) fn start_job(list: &std::sync::Arc<Vec<ThreadData>>, job: &mut Job) -> bool {
    let this_thread_index = tls_get_thread_index();
    list[this_thread_index]
        .job_queue
        .push(std::ptr::NonNull::new(job).unwrap())
}

/// Run the obtained job, and when it is finished queue any chained jobs
pub(crate) fn run_job(list: &std::sync::Arc<Vec<ThreadData>>, job: std::ptr::NonNull<Job>) {
    let resolved_job = unsafe {
        // This is safe since the memory address of the job is not going to be
        // moved/deallocate while the job system is running
        &mut *job.as_ptr()
    };
    {
        resolved_job.run();
        if let Some(parent_handle) = resolved_job.parent {
            unsafe {
                // This is safe since the memory address of the job is not going to be
                // moved/deallocate while the job system is running
                parent_handle.as_ref()
            }
            .unfinished_count
            .fetch_sub(1, std::sync::atomic::Ordering::Release);
        }
        let this_thread_index = tls_get_thread_index();
        let thread_data = &list[this_thread_index];
        for i in 0..resolved_job
            .chained_job_count
            .load(std::sync::atomic::Ordering::Acquire) as usize
        {
            let r = thread_data.job_queue.push(resolved_job.chained_jobs[i]);
            if !r {
                panic!("Failed to push chained job to job queue");
            }
        }
        resolved_job
            .chained_job_count
            .store(0, std::sync::atomic::Ordering::Release);
    }
}
