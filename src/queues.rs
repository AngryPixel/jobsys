#![allow(dead_code)]
///
/// Work Sealing Job Queue implementations
///
use crate::job::*;
use std::cell::UnsafeCell;
use std::sync::atomic::{AtomicI32, Ordering};
use std::sync::Mutex;

#[derive(Debug)]
struct GuardedData {
    bottom: i32,
    top: i32,
    jobs: Vec<std::ptr::NonNull<Job>>,
}

#[derive(Debug)]
pub(crate) struct WorkStealingJobQueue {
    guarded_data: Mutex<GuardedData>,
}

impl WorkStealingJobQueue {
    pub(crate) fn new(capacity: usize) -> Self {
        debug_assert!(capacity.is_power_of_two());
        let mut data = Vec::<_>::with_capacity(capacity);
        data.resize_with(capacity, || std::ptr::NonNull::dangling());
        Self {
            guarded_data: Mutex::new(GuardedData {
                bottom: 0,
                top: 0,
                jobs: data,
            }),
        }
    }

    pub(crate) fn push(&self, job: std::ptr::NonNull<Job>) -> bool {
        let mut data = self.guarded_data.lock().unwrap();
        if data.bottom.wrapping_sub(data.top) >= data.jobs.len() as i32 {
            return false;
        }
        let index = data.bottom as usize & (data.jobs.len() - 1);
        data.jobs[index] = job;
        data.bottom = data.bottom.wrapping_add(1);
        true
    }

    pub(crate) fn pop(&self) -> Option<std::ptr::NonNull<Job>> {
        let mut data = self.guarded_data.lock().unwrap();
        let diff = data.bottom.wrapping_sub(data.top);
        if diff <= 0 {
            return None;
        }
        data.bottom = data.bottom.wrapping_sub(1);
        let index = data.bottom as usize & (data.jobs.len() - 1);
        Some(data.jobs[index])
    }

    pub(crate) fn steal(&self) -> Option<std::ptr::NonNull<Job>> {
        let mut data = self.guarded_data.lock().unwrap();
        let diff = data.bottom.wrapping_sub(data.top);
        if diff <= 0 {
            return None;
        }
        let index = data.top as usize & (data.jobs.len() - 1);
        data.top = data.top.wrapping_add(1);
        Some(data.jobs[index])
    }
}

// The queue can be safely sent and accessed from multiple threads
unsafe impl Send for WorkStealingJobQueue {}
unsafe impl Sync for WorkStealingJobQueue {}

/// Lockless work stealing queue based on Dynamic Circular Work-Stealing Deque by David Chase and
/// Yossi Lev : https://www.dre.vanderbilt.edu/~schmidt/PDF/work-stealing-dequeue.pdf
#[derive(Debug)]
pub(crate) struct LocklessWorkStealingJobQueue {
    bottom: AtomicI32,
    top: AtomicI32,
    jobs: Box<[UnsafeCell<std::ptr::NonNull<Job>>]>,
}
impl LocklessWorkStealingJobQueue {
    pub(crate) fn new(capacity: usize) -> Self {
        debug_assert!(capacity.is_power_of_two());
        let mut data = Vec::<_>::with_capacity(capacity);
        data.resize_with(capacity, || UnsafeCell::new(std::ptr::NonNull::dangling()));
        Self {
            bottom: AtomicI32::new(0),
            top: AtomicI32::new(0),
            jobs: data.into_boxed_slice(),
        }
    }

    pub(crate) fn push(&self, job: std::ptr::NonNull<Job>) -> bool {
        let bottom = self.bottom.load(Ordering::Relaxed);
        let top = self.top.load(Ordering::Acquire);
        if bottom.wrapping_sub(top) >= self.jobs.len() as i32 {
            return false;
        }
        let index = bottom as usize & (self.jobs.len() - 1);
        unsafe {
            // This is safe since we have guaranteed through the atomic pointers
            // that we are allowed to write to this index.
            *self.jobs[index].get() = job;
        }
        self.bottom.store(bottom.wrapping_add(1), Ordering::Release);
        true
    }

    pub(crate) fn pop(&self) -> Option<std::ptr::NonNull<Job>> {
        let original_bottom = self.bottom.load(Ordering::SeqCst);
        let bottom = original_bottom.wrapping_sub(1);
        self.bottom.swap(bottom, Ordering::SeqCst);
        let top = self.top.load(Ordering::SeqCst);
        let diff = bottom.wrapping_sub(top);
        if diff >= 0 {
            // non empty queue
            let index = bottom as usize & (self.jobs.len() - 1);
            let job = unsafe {
                // This block is safe since we have guaranteed with the atomic operations above
                // that we are allowed to access this index
                *self.jobs[index].get()
            };
            if diff > 0 {
                // More than one job left
                return Some(job);
            }
            // Exactly one job left. We need to update top as well to ensure that only one of pop()
            // or steal() can access the last element in the queue.
            let mut result: Option<std::ptr::NonNull<Job>> = None;
            let next_top = top.wrapping_add(1);
            if self
                .top
                .compare_exchange(top, next_top, Ordering::SeqCst, Ordering::Relaxed)
                .is_ok()
            {
                // We wont he compare exchange, steal() did not get the job
                result = Some(job);
            }

            // Regardless of the outcome we still need to update bottom to be the same as top + 1
            // to match the empty queue setting
            self.bottom.store(next_top, Ordering::Relaxed);
            result
        } else {
            // Queue is empty, we set the value of bottom to top
            self.bottom.store(top, Ordering::Relaxed);
            None
        }
    }

    pub(crate) fn steal(&self) -> Option<std::ptr::NonNull<Job>> {
        let top = self.top.load(Ordering::SeqCst);
        let bottom = self.bottom.load(Ordering::SeqCst);
        let diff = bottom.wrapping_sub(top);
        if diff > 0 {
            let index = top as usize & (self.jobs.len() - 1);
            // Read job before compare exchange to safe guard against it being overwritten
            // after too many push() calls
            let job = unsafe {
                // This block is safe since we have guaranteed with the atomic operations above
                // that we are allowed to access this index
                *self.jobs[index].get()
            };
            if self
                .top
                .compare_exchange(
                    top,
                    top.wrapping_add(1),
                    Ordering::SeqCst,
                    Ordering::Relaxed,
                )
                .is_err()
            {
                // We lost the compare exchange with pop,
                return None;
            }
            Some(job)
        } else {
            None
        }
    }
}

// The queue can be safely sent and accessed from multiple threads
unsafe impl Send for LocklessWorkStealingJobQueue {}
unsafe impl Sync for LocklessWorkStealingJobQueue {}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn wrapping_indices() {
        let mut queue = WorkStealingJobQueue::new(1024);
        {
            let mut access = queue.guarded_data.get_mut().unwrap();
            access.bottom = i32::MAX - 1;
            access.top = i32::MAX - 1;
        }
        let job = std::ptr::NonNull::dangling();

        assert!(queue.push(job));
        assert!(queue.push(job));
        assert!(queue.push(job));
        assert!(queue.push(job));

        assert!(queue.pop().is_some());
        assert!(queue.steal().is_some());
        assert!(queue.pop().is_some());
        assert!(queue.steal().is_some());
    }

    #[test]
    fn wrapping_indices_lockless() {
        let queue = LocklessWorkStealingJobQueue::new(1024);
        {
            queue.bottom.store(i32::MAX - 1, Ordering::Release);
            queue.top.store(i32::MAX - 1, Ordering::Release);
        }
        let job = std::ptr::NonNull::dangling();

        assert!(queue.push(job));
        assert!(queue.push(job));
        assert!(queue.push(job));
        assert!(queue.push(job));

        assert!(queue.pop().is_some());
        assert!(queue.steal().is_some());
        assert!(queue.pop().is_some());
        assert!(queue.steal().is_some());
    }

    #[test]
    fn lockless_queue_full() {
        let queue = LocklessWorkStealingJobQueue::new(4);
        let job = std::ptr::NonNull::dangling();
        assert!(queue.push(job));
        assert!(queue.push(job));
        assert!(queue.push(job));
        assert!(queue.push(job));
        assert!(!queue.push(job));
    }
}
