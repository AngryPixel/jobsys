# JobSys

## About
Provides an implementation of a Lockless Work Stealing Job System which, after initialization, 
does not consume additional memory to schedule jobs.

This crate requires rust edition 2018 and can be run with stable. It has been tested with rust version 1.50.0.

For more details, please consult the [crate's documentation](https://docs.rs/jobsys/latest/jobsys)

**Note**: It's recommended to use version 0.2.0 going forward as it fixes many issues in the original release.

## Example

```rust
use jobsys::{JobSystem, JobScope, JobInstance};

fn main() {
    let thread_count = 4_usize;
    let job_capacity = 512_usize;
    let job_sys = JobSystem::new(thread_count, job_capacity).unwrap();
    let job_scope = JobScope::new_from_system(&job_sys);
    let job_instance = JobInstance::create(&job_scope, || {
        println!("Hello from Job Instance");
        }).unwrap();
    job_instance.wait_with(|| println!("Waiting on Job to Finish")).expect("Failed to wait on job");
}
```
